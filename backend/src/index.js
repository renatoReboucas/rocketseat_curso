var express = require('express');
var mongoose = require('mongoose');
const cors = require('cors');

var app = express();

const server = require('http').Server(app);
const io = require('socket.io')(server);

mongoose.connect('mongodb://user:mudar123@localhost:8081/test',
{
    useNewUrlParser:true
}
);

app.use((req, res, next) => {
    req.io = io;
    return next();
});
app.use(cors());
app.use(express.json());
app.use(require('./routes'));

server.listen(3000, function () {
    console.log(' :) Example app listening on port 3000!');
});

